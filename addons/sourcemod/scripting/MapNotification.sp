#include <sourcemod>
#include <Discord>

#define PLUGIN_VERSION "1.101"

#pragma newdecls required

ConVar g_cvNetPublicAddr, g_cvPort;

public Plugin myinfo = 
{
    name = "MapNotification",
    author = "maxime1907",
    description = "Sends a server info message to discord on map start",
    version = PLUGIN_VERSION,
    url = "https://nide.gg"
};

public void OnPluginStart()
{
    g_cvNetPublicAddr = FindConVar("net_public_adr");
    if (g_cvNetPublicAddr == null)
		g_cvNetPublicAddr = CreateConVar("sm_mapnotification_ip", "0.0.0.0", "Enter server IP in case of no detection", FCVAR_PROTECTED);
		
    g_cvPort = FindConVar("hostport");
    if (g_cvPort == null)
		g_cvPort = CreateConVar("sm_mapnotification_port", "27000", "Enter server PORT in case of no detection", FCVAR_PROTECTED);

    AutoExecConfig(true);
}
public void OnMapStart()
{
    char sTimeFormatted[64];
    char sTime[128];
    int iTime = GetTime();
    FormatTime(sTimeFormatted, sizeof(sTimeFormatted), "%m/%d/%Y @ %H:%M:%S", iTime);
    Format(sTime, sizeof(sTime), "Date: **%s**", sTimeFormatted);

    char sMapName[PLATFORM_MAX_PATH];
    char sMap[PLATFORM_MAX_PATH+64];
    GetCurrentMap(sMapName, sizeof(sMapName));
    Format(sMap, sizeof(sMap), "Map: **%s**", sMapName);

    char sPlayerCount[64];
    int iClients = GetClientCount(false);
    Format(sPlayerCount, sizeof(sPlayerCount), "Players: **%d/%d**", iClients, MaxClients);

    char sQuickJoin[255];
    char sIP[32];
    char sPort[32];
    GetConVarString(g_cvNetPublicAddr, sIP, sizeof(sIP));
    GetConVarString(g_cvPort, sPort, sizeof (sPort));
    Format(sQuickJoin, sizeof(sQuickJoin), "Quick join: **steam://connect/%s:%s**", sIP, sPort);

    char sWebhook[64];
    Format(sWebhook, sizeof(sWebhook), "mapnotification");

    char sMessage[4096];
    Format(sMessage, sizeof(sMessage), ">>> %s\n%s\n%s\n%s", sMap, sPlayerCount, sQuickJoin, sTime);

    Discord_SendMessage(sWebhook, sMessage);
}